package attractor.kg.rating_system.controller;

import attractor.kg.rating_system.model.User;
import attractor.kg.rating_system.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@AllArgsConstructor
public class UserController {
    private UserService userService;

    @GetMapping("/register")
    public String getRegisterPage(){
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(User user){
        userService.registerUser(user);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String getLoginPage(){
        return "login";
    }


    @GetMapping("/")
    public String redirectToMain(){
        return "redirect:/places";
    }
}