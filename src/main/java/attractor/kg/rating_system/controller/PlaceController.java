package attractor.kg.rating_system.controller;

import attractor.kg.rating_system.dto.ReviewDto;
import attractor.kg.rating_system.model.Image;
import attractor.kg.rating_system.model.Place;
import attractor.kg.rating_system.model.Review;
import attractor.kg.rating_system.service.ImageService;
import attractor.kg.rating_system.service.PlaceService;
import attractor.kg.rating_system.service.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class PlaceController {
    private final PlaceService placeService;
    private final ImageService imageService;
    private final ReviewService reviewService;

    @GetMapping("/places")
    public String getMainPage(Authentication authentication, Model model){
        if(!model.containsAttribute("places")){
            List<Place> places = placeService.getAllPlaces();
            model.addAttribute("places", placeService.makePlacesDto(places));
        }
        try {
            String username = authentication.getName();
            model.addAttribute("user", username);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return "places";
    }

    @GetMapping("/add_place")
    public String getAddPlacePage(Authentication authentication, Model model){
        model.addAttribute("user", authentication.getName());
        return "add_place";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/add_place")
    public String addPlace(@RequestParam("title") String title,
                             @RequestParam("description") String description,
                             @RequestParam("image") MultipartFile file) {
        Place addedPlace = placeService.savePlace(title, description);
        imageService.saveImage(file, addedPlace);
        return "redirect:/places";
    }

    @GetMapping("/place/{placeId}")
    public String getPlacePage(@PathVariable("placeId") int placeId, Model model){
        Place place = placeService.getPlaceById(placeId);
        List<Review> reviews = reviewService.findAllByPlaceId(placeId);
        List<Image> images = imageService.getImagesByPlaceId(placeId);

        model.addAttribute("place", placeService.makePlaceDto(place));
        model.addAttribute("reviews", reviews.stream().map(ReviewDto::from).collect(Collectors.toCollection(ArrayList::new)));
        model.addAttribute("images", images);

        return "place";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/upload_image")
    public String uploadImage(@RequestParam("image") MultipartFile file,
                              @RequestParam("place_id") int placeId){
        Place place = placeService.getPlaceById(placeId);
        imageService.saveImage(file, place);
        return "redirect:/place/" + placeId;
    }

    @PostMapping("/search")
    public String searchPlaces(@RequestParam("search_word") String searchWord, RedirectAttributes attributes){
        List<Place> places = placeService.getSearchedPlaces(searchWord);
        attributes.addFlashAttribute("places", placeService.makePlacesDto(places));
        return "redirect:/places";
    }
}
