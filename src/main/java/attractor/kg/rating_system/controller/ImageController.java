package attractor.kg.rating_system.controller;

import attractor.kg.rating_system.model.Image;
import attractor.kg.rating_system.service.ImageService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@AllArgsConstructor
public class ImageController {
    private final ImageService imageService;

    @GetMapping("/images/{imageId}")
    @ResponseBody
    public ResponseEntity<byte[]> getImage(@PathVariable("imageId") int imageId) {
        Image img = imageService.getImage(imageId);
        final MediaType mediaType = img.getFileType().toLowerCase().contains("png") ? MediaType.IMAGE_PNG : MediaType.IMAGE_JPEG;
        try {
            return ResponseEntity
                    .ok()
                    .contentType(mediaType)
                    .body(img.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/images/some_place", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public void getImage(HttpServletResponse response) throws IOException {
        var imgFile = new ClassPathResource("/static/images/some_place.png");
        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        StreamUtils.copy(imgFile.getInputStream(), response.getOutputStream());
    }
}
