package attractor.kg.rating_system.controller;

import attractor.kg.rating_system.service.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@AllArgsConstructor
public class ReviewController {
    private ReviewService reviewService;

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/add_review")
    public String addReview(@RequestParam("comment") String comment,
                            @RequestParam("rating") int rating,
                            @RequestParam("place_id") int placeId,
                            Authentication authentication){
        reviewService.saveReview(placeId, rating, comment, authentication);
        return "redirect:/place/" + placeId;
    }
}
