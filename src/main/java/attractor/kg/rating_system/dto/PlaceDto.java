package attractor.kg.rating_system.dto;

import attractor.kg.rating_system.model.Place;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PlaceDto {
    private int id;
    private String title;
    private String description;
    private int imageId;
    private float rating;
    private int reviewsAmount;

    public static PlaceDto from(Place place, int imageId) {
        return builder()
                .id(place.getId())
                .title(place.getTitle())
                .description(place.getDescription())
                .imageId(imageId)
                .rating((float) Math.round(place.countRating() * 10) / 10)
                .reviewsAmount(place.getReviews().size())
                .build();
    }
}
