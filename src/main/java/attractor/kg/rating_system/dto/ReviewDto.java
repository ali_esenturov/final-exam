package attractor.kg.rating_system.dto;

import attractor.kg.rating_system.model.Review;
import lombok.Builder;
import lombok.Data;

import java.time.format.DateTimeFormatter;

@Data
@Builder
public class ReviewDto {
    private int id;
    private int rating;
    private String ldt;
    private String comment;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private String user;

    public static ReviewDto from(Review review) {
        return builder()
                .id(review.getId())
                .rating(review.getRating())
                .comment(review.getComment())
                .ldt(review.getLdt().format(formatter))
                .user(review.getUser().getEmail())
                .build();
    }
}
