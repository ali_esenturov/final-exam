package attractor.kg.rating_system.service;

import attractor.kg.rating_system.exception.ImageNotFoundException;
import attractor.kg.rating_system.exception.ImageSavingException;
import attractor.kg.rating_system.model.Image;
import attractor.kg.rating_system.model.Place;
import attractor.kg.rating_system.repository.ImageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@AllArgsConstructor
public class ImageService {
    private ImageRepository imageRepository;

    public void saveImage(MultipartFile file, Place place) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if(fileName.contains("..")) {
                throw new ImageSavingException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            Image image = new Image();
            image.setFileName(fileName);
            image.setFileType(file.getContentType());
            image.setData(file.getBytes());
            image.setPlace(place);
            imageRepository.save(image);
        } catch (IOException ex) {
            throw new ImageSavingException("Could not save image " + fileName + ". Please try again!");
        }
    }

    public Image getImage(int imageId) {
        return imageRepository.findById(imageId)
                .orElseThrow(() -> new ImageNotFoundException("File not found with id " + imageId));
    }

    public List<Image> getImagesByPlaceId(int placeId){
        return imageRepository.findByPlaceId(placeId);
    }
}
