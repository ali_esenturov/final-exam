package attractor.kg.rating_system.service;

import attractor.kg.rating_system.model.Place;
import attractor.kg.rating_system.model.Review;
import attractor.kg.rating_system.model.User;
import attractor.kg.rating_system.repository.PlaceRepository;
import attractor.kg.rating_system.repository.ReviewRepository;
import attractor.kg.rating_system.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class ReviewService {
    ReviewRepository reviewRepository;
    PlaceRepository placeRepository;
    UserRepository userRepository;

    public void saveReview(int placeId, int rating, String comment, Authentication authentication){
        Place place = placeRepository.findById(placeId).get();
        User user = userRepository.findByEmail(authentication.getName());
        Review r = new Review();
        r.setPlace(place);
        r.setUser(user);
        r.setComment(comment);
        r.setRating(rating);
        r.setLdt(LocalDateTime.now());
        reviewRepository.save(r);
    }

    public List<Review> findAllByPlaceId(int placeId){
        return reviewRepository.findAllByPlaceIdOrderByLdtDesc(placeId);
    }
}
