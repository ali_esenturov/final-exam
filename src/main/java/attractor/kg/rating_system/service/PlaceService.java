package attractor.kg.rating_system.service;

import attractor.kg.rating_system.dto.PlaceDto;
import attractor.kg.rating_system.model.Image;
import attractor.kg.rating_system.model.Place;
import attractor.kg.rating_system.repository.ImageRepository;
import attractor.kg.rating_system.repository.PlaceRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class PlaceService {
    private final PlaceRepository placeRepository;
    private final ImageRepository imageRepository;

    public Place savePlace(String title, String description){
        Place place = new Place();
        place.setTitle(title);
        place.setDescription(description);
        return placeRepository.save(place);
    }

    public List<Place> getAllPlaces(){
        return placeRepository.findAll();
    }

    public List<PlaceDto> makePlacesDto(List<Place> places){
        List<PlaceDto> placesDto = new ArrayList<>();
        for(Place p : places){
            placesDto.add(makePlaceDto(p));
        }
        return placesDto;
    }

    public PlaceDto makePlaceDto(Place place){
        if(imageRepository.findFirstByPlaceId(place.getId()).isPresent()){
            Image image = imageRepository.findFirstByPlaceId(place.getId()).get();
            return PlaceDto.from(place, image.getId());
        }
        return PlaceDto.from(place, 0);
    }

    public Place getPlaceById(int placeId){
        return placeRepository.findById(placeId).get();
    }

    public List<Place> getSearchedPlaces(String searchWord){
        return placeRepository.findAllByTitleContainsOrDescriptionContains(searchWord, searchWord);
    }
}
