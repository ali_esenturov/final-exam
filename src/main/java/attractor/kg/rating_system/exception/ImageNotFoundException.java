package attractor.kg.rating_system.exception;

public class ImageNotFoundException extends RuntimeException {
    public ImageNotFoundException(String message){
        super(message);
    }
}