package attractor.kg.rating_system.exception;

import java.io.IOException;

public class ImageSavingException extends RuntimeException {
    public ImageSavingException(String message){
        super(message);
    }
}
