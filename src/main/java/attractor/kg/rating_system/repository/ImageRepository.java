package attractor.kg.rating_system.repository;

import attractor.kg.rating_system.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ImageRepository extends JpaRepository<Image, Integer> {
    Optional<Image> findFirstByPlaceId(int placeId);
    List<Image> findByPlaceId(int placeId);
}