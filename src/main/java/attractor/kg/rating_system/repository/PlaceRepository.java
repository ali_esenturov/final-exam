package attractor.kg.rating_system.repository;

import attractor.kg.rating_system.model.Place;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlaceRepository extends JpaRepository<Place, Integer> {
    List<Place> findAll();

    List<Place> findAllByTitleContainsOrDescriptionContains(String title, String description);
}