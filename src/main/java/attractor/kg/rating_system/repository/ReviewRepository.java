package attractor.kg.rating_system.repository;

import attractor.kg.rating_system.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer> {
    List<Review> findAllByPlaceIdOrderByLdtDesc(int placeId);
}
