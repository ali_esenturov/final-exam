package attractor.kg.rating_system.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Email
    @Column
    private String email;

    @NotBlank
    @Column
    private String password;

    @Column
    @Builder.Default
    private String role = "USER";

    @Column
    @Builder.Default
    private boolean enabled = true;
}
