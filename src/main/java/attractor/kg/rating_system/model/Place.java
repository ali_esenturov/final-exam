package attractor.kg.rating_system.model;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.websocket.server.ServerEndpoint;
import java.util.List;

@Getter
@Setter
@Builder
@Entity
@Table(name = "places")
@AllArgsConstructor
@NoArgsConstructor
public class Place {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @Column
    private String title;

    @NotBlank
    @Column
    private String description;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "place")
    private List<Review> reviews;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "place")
    private List<Image> images;

    public float countRating(){
        if(reviews.size() > 0){
            float sum = 0;
            for(Review r : reviews){
                sum += r.getRating();
            }
            return sum / reviews.size();
        } else{
            return 0;
        }
    }
}