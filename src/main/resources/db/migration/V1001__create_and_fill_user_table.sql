use rating_system;

CREATE TABLE users(
                id        int auto_increment NOT NULL,
                email     varchar(50)        NOT NULL,
                password  varchar(128)       NOT NULL,
                enabled   boolean            NOT NULL default true,
                role      varchar(10)        NOT NULL,
                PRIMARY KEY (id)
);

CREATE INDEX email_unique_index
    ON users (email);

insert into users(email, password, role)
values ('testuser_1@mail.ru', '$2a$10$XsE.RJOCer9euKMCvN8bKezoEdzI2guV9Ss275nTtx2jfHE9wnWUy', 'USER');

insert into users(email, password, role)
values ('testuser_2@mail.ru', '$2a$10$XsE.RJOCer9euKMCvN8bKezoEdzI2guV9Ss275nTtx2jfHE9wnWUy', 'USER');