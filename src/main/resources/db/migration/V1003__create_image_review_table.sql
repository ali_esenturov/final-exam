CREATE TABLE images(
                         id               int auto_increment     not null,
                         file_name        varchar(255)           not null,
                         file_type        varchar(255)           not null,
                         data             longblob               not null,
                         place_id         int                    not null,
                         primary key (id),
                         CONSTRAINT fk_image_place_id FOREIGN KEY (place_id) REFERENCES places (id)
);

CREATE TABLE reviews(
                        id              int auto_increment  NOT NULL,
                        user_id         int                 NOT NULL,
                        place_id        int                 NOT NULL,
                        rating          int                 NOT NULL,
                        comment         varchar(300)        NULL,
                        ldt             datetime            NOT NULL,
                        PRIMARY KEY (id),
                        CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (id),
                        CONSTRAINT fk_review_place_id FOREIGN KEY (place_id) REFERENCES places (id)
);