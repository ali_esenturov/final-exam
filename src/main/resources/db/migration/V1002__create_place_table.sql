use rating_system;

CREATE TABLE places(
                         id              int auto_increment         NOT NULL,
                         title           varchar(50)                NOT NULL,
                         description     varchar(500)               NOT NULL,
                         PRIMARY KEY (id)
);

CREATE INDEX place_title_index
    ON places (title);
CREATE INDEX place_description_index
    ON places (description);

insert into places (title, description)
values('Place 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 5', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 6', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 7', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 8', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 9', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 10', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 11', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 12', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 13', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 14', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 15', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 16', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 17', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 18', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 19', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 20', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 21', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 22', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 23', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 24', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 25', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 26', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 27', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 28', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 29', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');

insert into places (title, description)
values('Place 30', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima saepe sed sequi
voluptates. Amet, cumque earum explicabo nemo nesciunt odio provident. Ab error, fuga harum illo nobis officiis quia quisquam.');