Hello, developer or user!

To run and modify this application you need:
* Java
* Java Development Kit (JDK)
* Java Runtime Environment (JRE)
* One of the Integrated Development Environments (IDE)

This project worked on:
* Spring.framework.boot with 2.3.3. RELEASE version
* Java version need to be equal or more than 11

Dependencies:
* spring-boot-starter-data-jpa
* spring-boot-starter-freemarker
* spring-boot-starter-web
* spring-boot-starter-security
* spring-boot-starter-validation
* spring-boot-starter-test
* spring-boot-devtools
* mysql-connector-java
* flyway-core
* lombok
* junit-vintage-engine

To build the project you need:
* spring-boot-maven-plugin

Also create "rating_system" database in MySQL

Add your specific connection settings into application properties:
* spring.datasource.url
* spring.datasource.username
* spring.datasource.password

In the very beginning you can use test profile for login with
	* username: testuser_1@mail.ru
	* password: 111111
	or
	* username: testuser_2@mail.ru
	* password: 111111

For asking questions or making proposals, please contact me: aliesenturovvv@gmail.com